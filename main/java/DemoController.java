import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Test
 */

public class DemoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DbManager db = DbManager.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DemoController() {
		super();
	}

	private void resetPlanets() {

		db.deleteAllPlanet();

		db.insertPlanet(new Planet(0, "Венера"));
		db.insertPlanet(new Planet(0, "Земля"));
		db.insertPlanet(new Planet(0, "Марс"));
		db.insertPlanet(new Planet(0, "Меркурій"));
		db.insertPlanet(new Planet(0, "Нептун"));
		db.insertPlanet(new Planet(0, "Уран"));
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("GET: " + request.getRequestURI());

		response.setContentType("application/json; charset=UTF-8");
//		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS,HEAD");

		String[] path = request.getRequestURI().split("/");
		if (path.length < 2) {
			notFound(response);
		}

		if ("planets".equals(path[2])) {
			try {
				int pathId = retrievePathId(request);
				response.getWriter().append(getPlanet(pathId));
			} catch (Exception e) {
				response.getWriter().append(getPlanets());
			}
		} else if ("requests".equals(path[2])) {
			try {
				int pathId = retrievePathId(request);
				response.getWriter().append(getRequest(pathId));
			} catch (Exception e) {
				response.getWriter().append(getRequests());
			}
		} else if ("reset".equals(path[2])) {
			resetPlanets();
			response.getWriter().append(getPlanets());
		} else {
			notFound(response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("POST: " + request.getRequestURI());

		response.setContentType("application/json; charset=UTF-8");
//		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS,HEAD");

		String[] path = request.getRequestURI().split("/");
		if (path.length < 2) {
			notFound(response);
		}

		if ("planets".equals(path[2])) {
			try {
				response.getWriter().append(insertPlanet(request.getParameter("name")));
			} catch (Exception e) {
				response.getWriter().append("{}");
			}
		} else if ("requests".equals(path[2])) {
			try {
				response.getWriter().append(insertRequest(request));
			} catch (Exception e) {
				response.getWriter().append("{}");
			}
		} else {
			response.getWriter().append("{}");
		}
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		System.out.println("DELETE " + req.getRequestURI());

		resp.setContentType("application/json; charset=UTF-8");
//		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		resp.addHeader("Access-Control-Allow-Credentials", "true");
		resp.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS,HEAD");

		String[] path = req.getRequestURI().split("/");
		if (path.length < 2) {
			notFound(resp);
		}
		int pathId = retrievePathId(req);

		if ("planets".equals(path[2])) {
			try {
				String s = getPlanet(pathId);
				if (!db.deletePlanetById(pathId)) {
					s = "{}";
				}
				resp.getWriter().append(s);
			} catch (Exception e) {
				resp.getWriter().append("{}");
			}
		} else if ("requests".equals(path[2])) {
			try {
				String s = getRequest(pathId);
				if (!db.deleteRequestById(pathId)) {
					s = "{}";
				}
				resp.getWriter().append(s);
			} catch (Exception e) {
				resp.getWriter().append("{}");
			}
		} else {
			resp.getWriter().append("{}");
		}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		System.out.println("PUT: " + req.getRequestURI());

		resp.setContentType("application/json; charset=UTF-8");
//		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		resp.addHeader("Access-Control-Allow-Credentials", "true");
		resp.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS,HEAD");

		String[] path = req.getRequestURI().split("/");
		if (path.length < 2) {
			notFound(resp);
		}

		int pathId = retrievePathId(req);
		String body = inputStreamToString(req.getInputStream());
		Map params = parseBody(body);
		System.out.println(updateRequest(pathId, params));

		if ("planets".equals(path[2])) {
			try {
				resp.getWriter().append(updatePlanet(pathId, params));
			} catch (Exception e) {
				resp.getWriter().append("{}");
			}
		} else if ("requests".equals(path[2])) {
			try {
				resp.getWriter().append(updateRequest(pathId, params));
			} catch (Exception e) {
				resp.getWriter().append("{}");
			}
		} else {
			resp.getWriter().append("{}");
		}
	}

	protected int retrievePathId(HttpServletRequest req) {
		String pathInfo = req.getPathInfo();
		if (pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
		}
		return Integer.parseInt(pathInfo);
	}

	protected int retrieveCommand(HttpServletRequest req) {
		String pathInfo = req.getPathInfo();
		if (pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
		}
		return Integer.parseInt(pathInfo);
	}

	protected static String inputStreamToString(InputStream inputStream) {
		Scanner scanner = new Scanner(inputStream, "UTF-8");
		return scanner.hasNext() ? scanner.useDelimiter("\\A").next() : "";
	}

	protected void notFound(HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Not found");
	}

	protected String getPlanets() {
		List<Planet> list = db.findAllPlanets();
		if (list.isEmpty()) {
			return "{}";
		}
		StringBuffer sb = new StringBuffer("[");
		for (Planet planet : list) {
			sb.append(planet);
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	protected String getPlanet(int id) {
		Planet planet = db.findPlanetById(id);
		if (planet == null) {
			return "{}";
		}
		return planet.toString();
	}

	protected String updatePlanet(int id, Map params) {
		Planet planet = new Planet(id, (String) params.get("name"));
		if (!db.updatePlanetById(planet)) {
			return "{}";
		}
		return getPlanet(id);
	}

	protected String insertPlanet(String name) {
		Planet planet = new Planet(0, name);
		if (!db.insertPlanet(planet)) {
			return "{}";
		}
		return planet.toString();
	}

	protected String getRequests() {
		List<Request> list = db.findAllRequests();
		if (list.isEmpty()) {
			return "{}";
		}
		StringBuffer sb = new StringBuffer("[");
		for (Request request : list) {
			sb.append(request);
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	protected String getRequest(int id) {
		Request request = db.findRequestById(id);
		if (request == null) {
			return "{}";
		}
		return request.toString();
	}

	protected String updateRequest(int id, Map params) {
		Request request = db.findRequestById(id);
		if (request == null) {
			return "{}";
		}
		if (params.get("name") != null) {
			request.setName((String) params.get("name"));
		}
		if (params.get("tel") != null) {
			request.setTel((String) params.get("tel"));
		}
		if (params.get("planetId") != null) {
			request.setPlanetId(Integer.parseInt((String) params.get("planetId")));
		}
		if (params.get("status") != null) {
			request.setStatus((String) params.get("status"));
		}
		
		if (!db.updateRequestById(request)) {
			return "{}";
		}
		return getRequest(id);
	}

	protected String insertRequest(HttpServletRequest req) {
		
		Request request = new Request();
		request.setName((req.getParameter("name") != null) ? (String) req.getParameter("name") : "");
		request.setTel((req.getParameter("tel") != null) ? (String) req.getParameter("tel") : "");
		request.setPlanetId((req.getParameter("planetId") != null) ? Integer.parseInt(req.getParameter("planetId")) : 0);
		request.setStatus((req.getParameter("status") != null) ? (String) req.getParameter("status") : "");

		if (!db.insertRequest(request)) {
			return "{}";
		}
		return request.toString();
	}

	protected static Map<String, String> parseBody(String body) {

		Map<String, String> map = new HashMap<String, String>();

		body = body.replaceAll("\\\"", "");
		body = body.replaceAll("\\{", "");
		body = body.replaceAll("\\}", "");

		String[] lines = body.split(",");
		for (String s : lines) {
			String[] item = s.split(":");
			map.put(item[0], item[1]);
		}

		return map;
	}
}
