import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

class DemoControllerTest {

	@Test
	void testInputStreamToString() {
		String testStr = "{\"id\":2,\"name\":\"Земля\"}";
		InputStream is = null;
		try {
			is = new ByteArrayInputStream(testStr.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	
		String expected = "{\"id\":2,\"name\":\"Земля\"}";
		String result = DemoController.inputStreamToString(is);

		assertNotNull("Provided Sream is null;", is);
		
		assertNotNull("Provided String is null;", expected);

		assertNotNull("Received String is null;", result);

		assertEquals("Strings not equal;", expected, result);

	}

	@Test
	void testParseBody() {
		String testStr = "{\"id\":2,\"name\":\"Земля\"}";
		Map<String, String> expected = new HashMap<String, String>();
		expected.put("name", "Земля");
		expected.put("id", "2");

		Map<String, String> map = DemoController.parseBody(testStr);

		assertNotNull("Provided Map is null;", expected);

		assertNotNull("Received Map is null;", map);

		assertEquals("Size mismatch for maps;", expected.size(), map.size());

		assertTrue("Missing keys in received map;", map.keySet().containsAll(expected.keySet()));

		expected.keySet().stream().forEach((key) -> {
			assertEquals("Value mismatch for key '" + key + "';", expected.get(key), map.get(key));
		});

	}

}
