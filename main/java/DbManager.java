import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import com.mysql.cj.jdbc.MysqlDataSource;

public class DbManager {

	private static final String DB_URL = "jdbc:mysql://localhost:3306/marathon?user=root&password=12345";
	
	private static final String SQL_SELECT_FROM_PLANET_WHERE_ID = "SELECT * FROM planet WHERE id = ?";
	private static final String SQL_SELECT_FROM_REQUEST_WHERE_ID = "SELECT * FROM request WHERE id = ?";

	private static final String SQL_INSERT_PLANET = "INSERT INTO planet (name) VALUES (?)";
	private static final String SQL_INSERT_REQUEST = "INSERT INTO request (name, tel, planetId, status) VALUES (?, ?, ?, ?)";

	private static final String SQL_FIND_ALL_PLANET = "SELECT * FROM planet";
	private static final String SQL_FIND_ALL_REQUEST = "SELECT * FROM request";

	private static final String SQL_DELETE_ALL_PLANET = "TRUNCATE TABLE planet";
	private static final String SQL_DELETE_FROM_PLANET_WHERE_ID = "DELETE FROM planet WHERE id = ?";
	private static final String SQL_UPDATE_PLANET = "UPDATE planet SET name = ? WHERE id = ?";

	private static final String SQL_DELETE_FROM_REQUEST_WHERE_ID = "DELETE FROM request WHERE id = ?";
	private static final String SQL_UPDATE_REQUEST = "UPDATE request SET name = ?, tel = ?, planetId = ?, status = ? WHERE id = ?";

	private static DbManager instance = null;

	private DbManager() {

	}

	static public DbManager getInstance() {

		if (instance == null) {

			instance = new DbManager();
		}

		return instance;
	}

	public Connection getConnection() throws SQLException {
		
		return DriverManager.getConnection(DB_URL);

//		MysqlDataSource ds = new MysqlDataSource();
//		ds.setUrl("jdbc:mysql://localhost/marathon");
//		ds.setUser("root");
//		ds.setPassword("12345");
//		return ds.getConnection();
	}

	public List<Planet> findAllPlanets() {
		List<Planet> list = new ArrayList<Planet>();

		try (Connection con = getConnection()) {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(SQL_FIND_ALL_PLANET);

			while (rs.next()) {
				Planet planet = new Planet(rs.getInt(1), rs.getString("name"));
				list.add(planet);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	public Planet findPlanetById(int id) {
		Planet planet = null;

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_SELECT_FROM_PLANET_WHERE_ID);
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();

			if (rs.next()) {
				planet = new Planet(rs.getInt(1), rs.getString("name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return planet;
	}

	public boolean insertPlanet(Planet planet) {

		Connection con = null;

		try {
			con = getConnection();

			PreparedStatement pst = con.prepareStatement(SQL_INSERT_PLANET, Statement.RETURN_GENERATED_KEYS);

			pst.setString(1, planet.getName());

			if (pst.executeUpdate() > 0) {
				ResultSet rs = pst.getGeneratedKeys();
				if (rs.next()) {
					planet.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public void deleteAllPlanet() {

		try (Connection con = getConnection()) {
			Statement st = con.createStatement();
			st.execute(SQL_DELETE_ALL_PLANET);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean deletePlanetById(int id) {

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_DELETE_FROM_PLANET_WHERE_ID);
			pst.setInt(1, id);
			pst.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updatePlanetById(Planet planet) {

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_UPDATE_PLANET);
			pst.setString(1, planet.getName());
			pst.setInt(2, planet.getId());
			return (pst.executeUpdate() == 1);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void main(String[] args) {

		DbManager db = getInstance();

		List list = db.findAllPlanets();
		System.out.println(list);

		db.insertPlanet(new Planet(0, "name planet"));

		list = db.findAllPlanets();
		System.out.println(list);
	}

	public List<Request> findAllRequests() {
		List<Request> list = new ArrayList<Request>();

		try (Connection con = getConnection()) {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(SQL_FIND_ALL_REQUEST);

			while (rs.next()) {
				Request request = new Request(rs.getInt(1), rs.getString("name"), rs.getString("tel"), rs.getInt("planetId"), rs.getString("status"));
				list.add(request);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	public Request findRequestById(int id) {
		Request request = null;

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_SELECT_FROM_REQUEST_WHERE_ID);
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();

			if (rs.next()) {
				request = new Request(rs.getInt(1), rs.getString("name"), rs.getString("tel"), rs.getInt("planetId"), rs.getString("status"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return request;
	}

	public boolean insertRequest(Request request) {

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_INSERT_REQUEST, Statement.RETURN_GENERATED_KEYS);

			pst.setString(1, request.getName());
			pst.setString(2, request.getTel());
			pst.setInt(3, request.getPlanetId());
			pst.setString(4, request.getStatus());

			if (pst.executeUpdate() > 0) {
				ResultSet rs = pst.getGeneratedKeys();
				if (rs.next()) {
					request.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean deleteRequestById(int id) {

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_DELETE_FROM_REQUEST_WHERE_ID);
			pst.setInt(1, id);
			pst.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateRequestById(Request request) {

		try (Connection con = getConnection()) {
			PreparedStatement pst = con.prepareStatement(SQL_UPDATE_REQUEST);
			pst.setString(1, request.getName());
			pst.setString(2, request.getTel());
			pst.setInt(3, request.getPlanetId());
			pst.setString(4, request.getStatus());
			pst.setInt(5, request.getId());
			return (pst.executeUpdate() == 1);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}

class Planet {

	private int id;
	private String name;

	public Planet() {
		this.id = 0;
		this.name = "";
	}

	public Planet(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "{\"id\":" + id + ",\"name\":\"" + name + "\"}";
	}
}

class Request {

	private int id;
	private String name;
	private String tel;
	private int planetId;
	private String status;

	public Request() {
		this.id = 0;
		this.name = "";
		this.tel = "";
		this.planetId = 0;
		this.status = "";
	}

	public Request(int id, String name, String tel, int planetId, String status) {
		super();
		this.id = id;
		this.name = name;
		this.tel = tel;
		this.planetId = planetId;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getPlanetId() {
		return planetId;
	}

	public void setPlanetId(int planetId) {
		this.planetId = planetId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "{\"id\":" + id + ",\"name\":\"" + name + "\",\"tel\":\"" + tel + "\",\"planetId\":" + planetId
				+ ",\"status\":\"" + status + "\"}";
	}
	

}