-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema marathon
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `marathon` ;

-- -----------------------------------------------------
-- Schema marathon
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `marathon` DEFAULT CHARACTER SET utf8 ;
USE `marathon` ;

-- -----------------------------------------------------
-- Table `marathon`.`planet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `marathon`.`planet` ;

CREATE TABLE IF NOT EXISTS `marathon`.`planet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `marathon`.`request`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `marathon`.`request` ;

CREATE TABLE IF NOT EXISTS `marathon`.`request` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `tel` VARCHAR(45) NULL,
  `planetId` INT NULL,
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `marathon`.`planet`
-- -----------------------------------------------------
START TRANSACTION;
USE `marathon`;
INSERT INTO `marathon`.`planet` (`id`, `name`) VALUES (1, 'Венера');
INSERT INTO `marathon`.`planet` (`id`, `name`) VALUES (2, 'Земля');
INSERT INTO `marathon`.`planet` (`id`, `name`) VALUES (3, 'Марс');
INSERT INTO `marathon`.`planet` (`id`, `name`) VALUES (4, 'Меркурій');
INSERT INTO `marathon`.`planet` (`id`, `name`) VALUES (5, 'Нептун');
INSERT INTO `marathon`.`planet` (`id`, `name`) VALUES (6, 'Уран');

COMMIT;

